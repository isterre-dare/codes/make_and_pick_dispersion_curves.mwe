
------------------------------------------------------------------------

# Make and pick dispersion curves by clustering

## MWE for reading picked DCs and pre-computed FTAN panels, and fit them with some functions

## (smoothing splines for DCs, and Gaussians for estimating error bars)

Francois Lavoue (francois.lavoue-interim@irsn.fr), 4 July 2023  
Last updated Francois Lavoue (francois.lavoue@univ-grenoble-alpes.fr), 13 July 2023

Copyright 2023 IRSN  
Copyright 2023 Univ. Grenoble Alpes


------------------------------------------------------------------------

#### Requirements / Installation

This minimal working example (MWE) has minimal requirements and needs
only the following modules:
numpy, copy, sys, os, time, pandas.


------------------------------------------------------------------------

#### Notes (TO-DO list)

1. Fit picked dispersion curves with suitable functions (yet to defined,
   polynoms, splines, ...?).

   _Update (27/07/2023):_ Smoothing splines seem to do the job, e.g.  
   `from scipy.interpolate import splrep, BSpline`  
   `(https://docs.scipy.org/doc/scipy/tutorial/interpolate/smoothing_splines.html)`

   Fitting constraints:

   - Derivatives should be 0 at low and high frequency limits (flat curves at both ends)

   - We might want to impose a prior value at low frequency. The low-frequency
     limit of the group velocity $V_G(f)$ should correspond to the $V_S(z)$
     velocity of a deep halfspace.

   _Update (27/07/2023):_ The definition of this prior value is slightly
   more complicated than just a constant $V_G^{LF}$ value. Indeed, synthetic
   tests show that the group velocities of Love and Rayleigh fundamental
   modes at 0.1 Hz are still sensitive to shallow layers, which makes them
   dependent to the effective 1D model between the two stations. This prior
   $V_G^{LF}$ value must therefore be adapted for each station pair. In the
   future, such prior values could be estimated from a prior synthetic model.
   For now, we define this prior $V_G^{LF}$ value as the average of all
   picked Vg values located above the distance-wavelength threshold, for
   each station pair (although we know that these values are not very reliable).

2. Fit each column (for each frequency) of the averaged FTAN panel with
   a Gaussian-like function, in order to estimate the width of the "red
   zone", that will be considered as the error bar on the picked velocity
   value for each frequency.

   Fitting constraints:

   - Use a log-Gaussian function? (Yann's suggestion)

   - The maximum of the Gaussian should correspond to the final group
     velocity value for each frequency (we only look for the width of
     the Gaussian).

   - Maybe we could clean up the avg FTAN panel at high frequencies
     (remove amplitudes associated to higher modes) before trying to fit
     a function (to avoid over-estimating the width of the Gaussian).

   _Update (27/07/2023):_ This step is now implemented in branch `dev_FL`,
   using a Gaussian function (not log-Gaussian), and restricting the
   fitting to the range [ 0 , $3 Vg_{picked}^{final}$ ].

------------------------------------------------------------------------

### Further notes: final recipe

This the final(?) recipe we came up with for the QC of automatically-picked DC data,
and the definition of final DC data and of their uncertainties (error bars).

This recipe is implemented in branch `dev_FL`.

**0. Reminder of input data:** dispersion curves (DCs) obtained by automatic picking,  
   i.e. sets of group velocity values (y=Vg) for some frequencies (x=freq).

   We have 1 DC per component (comp) and correlation type (ctype),  
   i.e. up to 3 DCs per station pair for Love data (1 comp TT x 3 ctypes N+P+S),  
   and up to 6 DCs per station pair for Rayleigh data (2 comps RR+ZZ x 3 ctypes N+P+S).

**1. Clean and complement input data before 1st interpolation.**

**1.1.** Remove DC points that do not satisfy the condition `[interstation distance > k*wavelength]`.

   This correspond to the upper-left part of the FTAN panels [Vg=f(freq)],  
   i.e. to the low-frequency part of the DCs.

   NB: k is a parameter that may be tuned to define a threshold (k = 1,2,3,...).

**1.2.** Replace some of these data (for the lowest frequencies) with constant Vg values,
   in order to enforce a flat interpolation at low frequency, which is meant to
   represent a homogeneous halfspace at large depth.

   NB: we need to define the constant Vg value that will be enforced at low frequency.  

   This can be
   - the average of all picked Vg values above the distance threshold (although these data are not reliable...) *[preferred option #1, implemented]*
   - the average of a few input Vg values around the distance threshold
           (not recommended, because it tends to under-estimate the low-frequency Vg value)
   - a prior Vg value that may be estimated externally (e.g. from synthetics computed in a prior model). *[preferred option #2, to be implemented]*

**1.3.** Also add constant values at high-frequency,
   in order to enforce a flat interpolation at high frequency,
   which may represent our `blindness` to shallow vertical variations.

   NB: we need to define the constant Vg value that will be enforced at high frequency.  
       This can be the average of the few last (most HF) picked Vg values for the considered DC.

**2. First interpolation / spline fitting:** Fit (clean/complemented) DC points with smoothing splines
   (using `scipy.interpolate.splrep`), for each `comp` and `ctype` separately.

**3. Clean and complement input picked DC data after 1st and before 2nd interpolation.**

**3.1.** Compute the min/max envelopes of the obtained set of splines, and the average of the min and max.

**3.2.** Reject input picked data (and/or interpolated spline points) based on the min/max envelope, according to the following rule:

   `reject Vg values larger than [ avg(min-max) + Vg_thres ] above 1 Hz`

   This aims at rejecting potential higher modes that may pollute the automatic picking at high frequencies (> 1 Hz).

   NB: `Vg_thres` is a parameter that may be tuned to define a threshold (`Vg_thres = 100, 200, ... m/s`)

**3.3.** Again, replace some of the lowest-frequency data with a constant Vg value,
   in order to enforce a flat interpolation at low frequency.

   NB: The constant LF Vg value considered here may (/should?) be the same as for step 1.2.

**3.4.** Again, also add constant values at high-frequency, in order to enforce a flat interpolation.

   NB: The constant HF Vg value considered here may (/should?) be the same as for step 1.3.

   NB: For DCs where HF higher modes have been removed, this HF extrapolation may
       recreate an undesired higher mode with constant Vg. This must be remembered
       for cleaning the resulting splines before averaging them (step 5.1).

**4. Second interpolation / spline fitting:** Fit (clean/complemented) DC points with smoothing splines,
   for each comp and ctype separately (again, after removing "outliers").

   Now the resulting splines should be less biased towards HF higher modes,
   except maybe because of constant values extrapolated at HF.

**5. Clean and average the resulting splines**

**5.1.** For the DCs identified as containing higher modes at HF (step 3.2),
   replace HF values resulting from constant extrapolation with NaNs,
   to avoid biasing the average of the splines.

**5.2** For the DCs that do not contain original picked points above the threshold
   criterion, replace LF values (below the lowest frequency for which we have
   original picked points) by NaNs.

   NB: steps 5.1 and 5.2 aim at removing spline points that are not constrained
       by valid picked points, before averaging the splines.

**5.3.** Average all interpolated splines (using np.nanmean to handle NaNs).  
   This should give a smooth DC, except when the number of non-Nan spline
   points considered for the average varies because of steps 5.1 and 5.2
   (i.e. at 1 Hz, and potentially at some other frequencies).

**6. Third interpolation / spline fitting:** Fit the averaged spline with a smoothing spline,
   in order to well get a smooth final DC.

   NB: This way, we also get spline parameters that are useful in order to
       re-define the final DC for arbitrary sets of frequencies.

**7. Define error bars.**

**7.1.** Compute an average FTAN panel by stacking the FTAN panels for comps and ctypes.

**7.2.** For each frequency (/column) of the FTAN panel, fit the FTAN amplitudes
   with a Gaussian-like function centered on the final DC value (`Vg_final`),
   in order to estimate a sigma/std that corresponds to the width of the
   peak of FTAN amplitudes around this `Vg_final`. This sigma/std is our final
   error bar on our final Vg value.

------------------------------------------------------------------------

#### References

- Herrmann, R. B. (2013). Computer programs in seismology: An evolving tool
  for instruction and research, _Seismological Research Letters_ 84:1081-1088,
  doi:10.1785/0220110096.

- Lecocq, T., C. Caudron, and F. Brenguier (2014). MSNoise, a Python Package
  for Monitoring Seismic Velocity Changes Using Ambient Seismic Noise,
  _Seismological Research Letters_, 85(3):715-726, doi:10.1785/0220130073.

- Wathelet, M., Chatelain, J.L., Cornou, C., Di Giulio, G., Guillier, B.,
  Ohrnberger, M., and Savvaidis, A. (2020). Geopsy: A User-Friendly Open-
  Source Tool Set for Ambient Vibration Processing. _Seismological Research
  Letters_, 91(3):1878-1889, doi:10.1785/0220190360.

------------------------------------------------------------------------
